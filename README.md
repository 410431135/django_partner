# 師就處論壇

## About

伺服器位址：https://140.127.74.204

或 https://gostar4-f1.nknu.edu.tw

舊文件網址：https://140.127.74.204/docs

文件帳號：gostar

文件密碼：gostarnknu

### 開發環境

`python 3.6.3 + django 2.2.6 + MySql`

## Authors

**林冠曄** 410431135

- 高雄國立師範大學 數學系數學組108級

**李昱珊** 410531131

- 高雄國立師範大學 數學系數學組109級

(後面請自行增加)



