from django.db import models
import uuid
from django.utils import timezone
from django.contrib.auth.models import User
from article.models import Board

class ActivityGroup(models.Model):
	whichBoard = models.ForeignKey(Board, verbose_name = "所屬板塊", null = True, on_delete = models.SET_NULL, blank = True)
	whoCreate = models.ForeignKey(User, verbose_name = "群組創立人", null = True, on_delete = models.SET_NULL)
	idOfGroup = models.CharField(verbose_name = "活動群組編號", primary_key = True, max_length = 36,default = uuid.uuid4, editable = False)
	sGroupTitle = models.CharField(verbose_name = "活動群組名稱", max_length = 128, unique = True)
	sDescription = models.TextField(verbose_name = "活動群組描述", blank = True)
	# 狀態： XY. X:0公開(可看可加入)、1半公開(可看不能加入)、2不公開(不可看不能加入)
	iStatus = models.SmallIntegerField(verbose_name = "群組狀態", default = 2)
	isPublished = models.BooleanField(default = False, blank = True)
	createdTime = models.DateTimeField(verbose_name = "建立時間", default = timezone.now, editable = False)
	postTime = models.DateTimeField(verbose_name = "發表時間", null = True, blank = True)
	memberNumber = models.PositiveIntegerField(verbose_name = "群組人數", blank = True, null = True)

	def __str__(self):
		return "%s in %s" % (self.sGroupTitle, self.whichBoard)

class GroupSubtitle(models.Model):
	whichGroup = models.ForeignKey(ActivityGroup, verbose_name = "所屬活動群組", on_delete = models.CASCADE)
	sSubtitle = models.CharField(verbose_name = "子標題", max_length = 128)
	sDescription = models.TextField(verbose_name = "子標題描述", blank = True, null = True)
	bShowSubtitle = models.BooleanField(verbose_name = '顯示子標題', default = True, blank = True)

	def __str__(self):
		return "%s" % (self.sSubtitle)

class GroupThreeM(models.Model):
	whoLeft = models.ForeignKey(User, verbose_name = "黏貼的人", on_delete = models.SET_NULL, null = True)
	whichGroup = models.ForeignKey(ActivityGroup, verbose_name = "所屬活動群組", on_delete = models.CASCADE)
	whichSubtitle = models.ForeignKey(GroupSubtitle, verbose_name = "所屬子標題", on_delete = models.SET_NULL, null = True, blank = True)
	leftTime = models.DateTimeField(verbose_name = "黏貼時間", default = timezone.now, editable = False)
	sContent = models.TextField(verbose_name = "便利貼內容")

	def __str__(self):
		return "%s" % self.sContent


# 成員＆活動群組 many-to-manay
class UserOfActivityGroup(models.Model):
	iUserAccount = models.ForeignKey(User, verbose_name = "成員", on_delete = models.CASCADE)
	idOfGroup = models.ForeignKey(ActivityGroup, verbose_name = "所在活動群組", on_delete = models.CASCADE)

	def __str__(self):
		return "Member '%s' of Group '%s'" % (self.iUserAccount.get_full_name(), self.idOfGroup.sGroupTitle)

# 活動群組內的管理員群組
# 未來若要細分 可增加 has_xxx_permit
class has_all_permit(models.Model):
	idOfGroup = models.ForeignKey(ActivityGroup, verbose_name = "活動群組", on_delete = models.CASCADE)
	iUserAccount = models.ForeignKey(User, verbose_name = "活動群組管理員", on_delete = models.CASCADE)

	def __str__(self):
		return "Admin '%s' of Group '%s'" % (self.iUserAccount.get_full_name(), self.idOfGroup.sGroupTitle)


def grant_permit(group, user, permit):
	p = permit()
	p.idOfGroup = group
	p.iUserAccount = user
	p.save()

class TopicTag(models.Model):
	idOfTopic = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4)
	sTopic = models.CharField(verbose_name = '話題標籤', max_length = 128)

	def __str__(self):
		return self.sTopic

class ActivityGroupOfTopicTag(models.Model):
	idOfGroup = models.ForeignKey(ActivityGroup, on_delete = models.CASCADE)
	idOfTopic = models.ForeignKey(TopicTag, on_delete = models.CASCADE)

# 使用者留言文章
class CommentOfActivityGroup(models.Model):
	idOfGroup = models.ForeignKey(ActivityGroup, on_delete = models.CASCADE)
	iAccount = models.ForeignKey(User, on_delete = models.SET_NULL, null = True)
	idOfComment = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
	CommentTime = models.DateTimeField(verbose_name = '留言時間', default = timezone.now, editable = False)
	sContentOfComment = models.TextField(verbose_name = '留言內容')
	likeCommentNumber = models.PositiveIntegerField(verbose_name = "按讚數", blank = True, null = True)

	def __str__(self):
		return '%s 留言在 %s' % (self.iAccount, self.idOfGroup)

# 使用者收藏文章
class UserCollectActivityGroup(models.Model):
	idOfGroup = models.ForeignKey(ActivityGroup, on_delete = models.CASCADE)
	iAccount = models.ForeignKey(User, on_delete = models.CASCADE)

	def __str__(self):
		return '%s 收藏 %s' % (self.iAccount, self.idOfGroup)

# 使用者按讚文章
class UserLikesActivityGroup(models.Model):
	idOfGroup = models.ForeignKey(ActivityGroup, on_delete = models.CASCADE)
	iAccount = models.ForeignKey(User, on_delete = models.CASCADE)

	def __str__(self):
		return '%s 按讚 %s' % (self.iAccount, self.idOfGroup)

# 使用者按讚留言
class UserLikesGroupComment(models.Model):
	commentofActivityGroup = models.ForeignKey(CommentOfActivityGroup, on_delete = models.CASCADE)
	iAccount = models.ForeignKey(User, on_delete = models.CASCADE)