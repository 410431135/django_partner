from django.shortcuts import render, redirect
from django.http import HttpResponse
from ActivityGroup import forms
from django.contrib.auth.models import User
from ActivityGroup.models import *
from user.models import *


def browse(request, gClass = 'all'):
	user = request.user
	userList = list(User.objects.all())
	if user in userList:
		addmemberList = list(User.objects.all())
		addmemberList.remove(user)
		usergroupList = UserOfActivityGroup.objects.filter(iUserAccount = user)
		usergroupList = list(group.idOfGroup for group in usergroupList)

	addGroupform = forms.AddGroupForm(request.POST)
	if (gClass == 'all'):
		groupList = list(ActivityGroup.objects.all().order_by('-createdTime'))
		TagList = list(TopicTag.objects.all())
	elif (gClass == 'False'):
		groupList = list(ActivityGroup.objects.filter(isPublished = False).order_by('-createdTime'))
		TagList =[]
		Topic = []
		for group in groupList:
			if (ActivityGroupOfTopicTag.objects.filter(idOfGroup = group)):
				TagList = list(set(TagList).union(set(list(ActivityGroupOfTopicTag.objects.filter(idOfGroup = group)))))
				for i in TagList:
					Topic.append(i.idOfTopic)
		Topic = list(set(Topic))
	elif (gClass == 'True'):
		groupList = list(ActivityGroup.objects.filter(isPublished = True).order_by('-createdTime'))
		TagList =[]
		Topic = []
		for group in groupList:
			if (ActivityGroupOfTopicTag.objects.filter(idOfGroup = group)):
				TagList = list(set(TagList).union(set(list(ActivityGroupOfTopicTag.objects.filter(idOfGroup = group)))))
				for i in TagList:
					Topic.append(i.idOfTopic)
		Topic = list(set(Topic))
	elif (gClass == 'myGroup'):
		groupList = UserOfActivityGroup.objects.filter(iUserAccount = user)
		groupList = list(group.idOfGroup for group in groupList)
		TagList =[]
		Topic = []
		for group in groupList:
			if (ActivityGroupOfTopicTag.objects.filter(idOfGroup = group)):
				TagList = list(set(TagList).union(set(list(ActivityGroupOfTopicTag.objects.filter(idOfGroup = group)))))
				for i in TagList:
					Topic.append(i.idOfTopic)
		Topic = list(set(Topic))

	for group in groupList:
		group.memberNumber = len(UserOfActivityGroup.objects.filter(idOfGroup = group))
		group.save()


	if (request.method == 'POST' and addGroupform.is_valid()):
		error = ''
		sGroupTitle = addGroupform.cleaned_data['sGroupTitle']
		sDescription = addGroupform.cleaned_data['sDescription']
		iStatus = addGroupform.cleaned_data['iStatus']
		groupCheckSameList = [group.sGroupTitle for group in ActivityGroup.objects.all()]
		if (sGroupTitle in groupCheckSameList):
			error = '您輸入的內容與現有重複了'
			addGroupform = forms.AddGroupForm(request.POST)
			return render(request, 'ActivityGroup/activitygroup_browse.html', locals())
		group = ActivityGroup.objects.create(whoCreate = user, sGroupTitle = sGroupTitle, sDescription = sDescription, iStatus = iStatus)
		UserOfActivityGroup.objects.create(iUserAccount = user, idOfGroup = group)
		if (request.POST.get('select_member') != None):
			choice = request.POST.getlist('select_member')
			for i in choice:
				member = User.objects.get(username = i)
				UserOfActivityGroup.objects.create(iUserAccount = member, idOfGroup = group)
		addGroupform = forms.AddGroupForm(request.POST)
		return redirect('/activitygroup/isPublished=False/%s/discuss' % sGroupTitle)

	return render(request, 'ActivityGroup/activitygroup_browse.html', locals())

def search(request, gClass = 'all'):
	search = request.GET.get('search')
	user = request.user
	usergroupList = UserOfActivityGroup.objects.filter(iUserAccount = user)
	usergroupList = list(group.idOfGroup for group in usergroupList)
	if (gClass == 'all'):
		groupList_1 = list(ActivityGroup.objects.filter(sGroupTitle__contains=search))
		groupList_2 = list(ActivityGroup.objects.filter(sDescription__contains=search))
		groupList = list(set(groupList_1).union(set(groupList_2)))
		pieceOfdata = len(groupList)
	elif (gClass == 'False'):
		groupList_1 = list(ActivityGroup.objects.filter(sGroupTitle__contains=search, isPublished = False))
		groupList_2 = list(ActivityGroup.objects.filter(sDescription__contains=search, isPublished = False))
		groupList = list(set(groupList_1).union(set(groupList_2)))
		pieceOfdata = len(groupList)
	elif (gClass == 'True'):
		groupList_1 = list(ActivityGroup.objects.filter(sGroupTitle__contains=search, isPublished = True))
		groupList_2 = list(ActivityGroup.objects.filter(sDescription__contains=search, isPublished = True))
		groupList = list(set(groupList_1).union(set(groupList_2)))
		pieceOfdata = len(groupList)
	elif (gClass == 'myGroup'):
		groupList_1 = list(ActivityGroup.objects.filter(sGroupTitle__contains=search))
		groupList_2 = list(ActivityGroup.objects.filter(sDescription__contains=search))
		groupList_3 = usergroupList
		groupList = list(set(groupList_1).union(set(groupList_2)))
		groupList = list(set(groupList).intersection(set(groupList_3)))
		pieceOfdata = len(groupList)
		
	return render(request, 'ActivityGroup/activitygroup_browse.html', locals())

def hashtag(request, sTopic = 1):
	userList = list(User.objects.all())
	user = request.user
	userList.remove(user)
	addGroupform = forms.AddGroupForm(request.POST)
	usergroupList = UserOfActivityGroup.objects.filter(iUserAccount = user)
	usergroupList = list(group.idOfGroup for group in usergroupList)
	if (sTopic == 1):
		return redirect('/activitygroup')
	try:
		topic = TopicTag.objects.get(sTopic = sTopic)
	except:
		return HttpResponse("該活動群組不存在")
	groupList = list(ActivityGroupOfTopicTag.objects.filter(idOfTopic = topic))
	groupList = list(g.idOfGroup for g in groupList)

	return render(request, 'ActivityGroup/activitygroup_browse.html', locals())

def info(request, gClass = False, sGroupTitle = 1):
	# return HttpResponse("這個頁面可以看到特定群組的資訊")
	if (gClass == 1 and sGroupTitle == 1):
		return redirect('/activitygroup')
	group = ActivityGroup.objects.get(sGroupTitle = sGroupTitle)
	subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group))
	UnclassifiedthreeMList = GroupThreeM.objects.filter(whichGroup = group, whichSubtitle = None)
	user = request.user
	userinActivityGroup = UserOfActivityGroup.objects.filter(idOfGroup = group)
	userinActivityGroup = [user.iUserAccount for user in userinActivityGroup]
	if user in userinActivityGroup:
		return redirect('/activitygroup/class=%s/%s/discuss' % (gClass, sGroupTitle))

	if (request.method == 'POST'):
		if group.iStatus == 2:
			UserOfActivityGroup.objects.create(iUserAccount = user, idOfGroup = group)
			return redirect('/activitygroup/class=False/%s' % sGroupTitle)

	return render(request, 'ActivityGroup/activitygroup_info.html', locals())

def discuss(request, gClass = False, sGroupTitle = 1, sSubtitle = 'all'):
	# return HttpResponse("這個頁面是進入群組，可以參與討論")
	# need to handle exception if sGroupTitle not exist
	if (gClass != 1 and sGroupTitle != 1 and sSubtitle != 'all'):
		group = ActivityGroup.objects.get(sGroupTitle = sGroupTitle)
		if (sSubtitle != 'none'):
			uuidOfGroupSubtitle = GroupSubtitle.objects.get(whichGroup = group, sSubtitle = sSubtitle)

	if (sSubtitle == 'all'):
		group = ActivityGroup.objects.get(sGroupTitle = sGroupTitle)
		subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
		closesubtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = False))
		if subtitleList:
			return redirect('/activitygroup/class=%s/%s/discuss/%s' % (gClass, sGroupTitle, subtitleList[0]))
		else:
			UnclassifiedthreeMList = GroupThreeM.objects.filter(whichGroup = group, whichSubtitle = None)
			subtitleCheckSameList = [subtitle.sSubtitle for subtitle in GroupSubtitle.objects.filter(whichGroup = group)]
			allsubtitle = list(GroupSubtitle.objects.filter(whichGroup = group))
			# 增便利貼
			editThreeMform = forms.EditThreeMForm(request.POST)
			if (request.method == 'POST' and editThreeMform.is_valid()):
				error = ''
				sContentOf3m = editThreeMform.cleaned_data['sThreeMContent']
				if (sContentOf3m in UnclassifiedthreeMList):
					error = '您輸入的內容與現有重複了'
					editThreeMform = forms.EditThreeMForm(request.POST)
					return render(request, 'ActivityGroup/activitygroup_discuss.html', locals())
				threeM = GroupThreeM.objects.create(whoLeft = request.user, whichGroup = group, sContent = sContentOf3m)
				editThreeMform = forms.EditThreeMForm(request.POST)
				subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
				return redirect('/activitygroup/class=%s/%s/discuss' % (gClass, sGroupTitle))

			# 增子標題
			editSubtitleform = forms.EditSubtitleForm(request.POST)
			if (request.method == 'POST' and editSubtitleform.is_valid()):
				error = ''
				if not editSubtitleform.is_valid():
					error += editSubtitleform.errors['sSubtitle']

				sSubtitle = editSubtitleform.cleaned_data['sSubtitle']
				sDescription = editSubtitleform.cleaned_data['sDescription']
				if (sSubtitle in subtitleCheckSameList):
					error = '您輸入的標題與現有重複了'
					editSubtitleform = forms.EditSubtitleForm(request.POST)
					return render(request, 'ActivityGroup/activitygroup_discuss.html', locals())
				subtitle = GroupSubtitle.objects.create(whichGroup = group, sSubtitle = sSubtitle, sDescription = sDescription)
				editSubtitleform = forms.EditSubtitleForm(request.POST)
				subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
				return redirect('/activitygroup/class=%s/%s/discuss/%s' % (gClass, sGroupTitle, subtitleList[0]))

			# 刪便利貼
			if (request.method == 'POST' and request.POST.get('toSubtitle') != None):
				choice = request.POST.get('toSubtitle')
				selectedThreeM = request.POST.getlist('checkedThreeM')
				# 若選擇刪除
				if (choice == 'delete'):
					for threeM in selectedThreeM:
						obj = GroupThreeM.objects.get(whichGroup = group, sContent = threeM)
						obj.delete()
				return render(request, 'ActivityGroup/activitygroup_discuss.html', locals())

			if (request.method == 'POST' and request.POST.get('closesubtitle') != None):
				choice = request.POST.get('closesubtitle')
				obj = GroupSubtitle.objects.get(whichGroup = group, sSubtitle = choice)
				obj.bShowSubtitle = 'False'
				obj.save()
				subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
				return redirect('/activitygroup/class=%s/%s/discuss' % (gClass, sGroupTitle))

			if (request.method == 'POST' and request.POST.get('opensubtitle') != None):
				choice = request.POST.get('opensubtitle')
				obj = GroupSubtitle.objects.get(whichGroup = group, sSubtitle = choice)
				obj.bShowSubtitle = 'True'
				obj.save()
				subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
				return redirect('/activitygroup/class=%s/%s/discuss' % (gClass, sGroupTitle))

			if (request.method == 'POST' and request.POST.get('deletesubtitle') != None):
				choice = request.POST.get('deletesubtitle')
				obj = GroupSubtitle.objects.get(whichGroup = group, sSubtitle = choice)
				obj.delete()
				subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
				return redirect('/activitygroup/class=%s/%s/discuss/%s' % (gClass, sGroupTitle, subtitleList[0]))

			return render(request, 'ActivityGroup/activitygroup_discuss.html', locals())

	group = ActivityGroup.objects.get(sGroupTitle = sGroupTitle)
	threeContentMList = [threeM.sContent for threeM in GroupThreeM.objects.filter(whichGroup = group)]
	UnclassifiedthreeMList = GroupThreeM.objects.filter(whichGroup = group, whichSubtitle = None)
	threeMList = GroupThreeM.objects.filter(whichGroup = group, whichSubtitle = uuidOfGroupSubtitle)
	subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
	subtitleCheckSameList = [subtitle.sSubtitle for subtitle in GroupSubtitle.objects.filter(whichGroup = group)]
	allsubtitle = list(GroupSubtitle.objects.filter(whichGroup = group))

	# 貼便利貼
	editThreeMform = forms.EditThreeMForm(request.POST)
	if (request.method == 'POST' and editThreeMform.is_valid()):
		error = ''
		sContentOf3m = editThreeMform.cleaned_data['sThreeMContent']
		if (sContentOf3m in threeContentMList):
			error = '您輸入的內容與現有重複了'
			editThreeMform = forms.EditThreeMForm(request.POST)
			return render(request, 'ActivityGroup/activitygroup_discuss.html', locals())
		threeM = GroupThreeM.objects.create(whoLeft = request.user, whichGroup = group, sContent = sContentOf3m)
		editThreeMform = forms.EditThreeMForm(request.POST)
		subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
		return redirect('/activitygroup/class=%s/%s/discuss/%s' % (gClass, sGroupTitle, subtitleList[0]))

	# 增子標題
	editSubtitleform = forms.EditSubtitleForm(request.POST)
	if (request.method == 'POST' and editSubtitleform.is_valid()):
		error = ''
		if not editSubtitleform.is_valid():
			error += editSubtitleform.errors['sSubtitle']

		sSubtitle = editSubtitleform.cleaned_data['sSubtitle']
		sDescription = editSubtitleform.cleaned_data['sDescription']
		if (sSubtitle in subtitleCheckSameList):
			error = '您輸入的標題與現有重複了'
			editSubtitleform = forms.EditSubtitleForm(request.POST)
			return render(request, 'ActivityGroup/activitygroup_discuss.html', locals())
		subtitle = GroupSubtitle.objects.create(whichGroup = group, sSubtitle = sSubtitle, sDescription = sDescription)
		editSubtitleform = forms.EditSubtitleForm(request.POST)
		subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
		return redirect('/activitygroup/class=%s/%s/discuss/%s' % (gClass, sGroupTitle, subtitleList[0]))

	# 將便利貼送入子標題
	if (request.method == 'POST' and request.POST.get('toSubtitle') != None):
		choice = request.POST.get('toSubtitle')
		selectedThreeM = request.POST.getlist('checkedThreeM')
		# 若選擇刪除
		if (choice == 'delete'):
			for threeM in selectedThreeM:
				obj = GroupThreeM.objects.get(whichGroup = group, sContent = threeM)
				obj.delete()
		# 若要送回
		elif (choice == 'sendBack'):
			for threeM in selectedThreeM:
				obj = GroupThreeM.objects.get(whichGroup = group, sContent = threeM)
				obj.whichSubtitle = None
				obj.save()
		else:
			selectedSubtitle = GroupSubtitle.objects.get(whichGroup = group, sSubtitle = choice)
			for threeM in selectedThreeM:
				obj = GroupThreeM.objects.get(whichGroup = group, sContent = threeM)
				obj.whichSubtitle = selectedSubtitle
				obj.save()

		return render(request, 'ActivityGroup/activitygroup_discuss.html', locals())

	# 將便利貼從子標題內拿出

	if (request.method == 'POST' and request.POST.get('closesubtitle') != None):
		choice = request.POST.get('closesubtitle')
		obj = GroupSubtitle.objects.get(whichGroup = group, sSubtitle = choice)
		obj.bShowSubtitle = 'False'
		obj.save()
		subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
		return redirect('/activitygroup/class=%s/%s/discuss' % (gClass, sGroupTitle))

	if (request.method == 'POST' and request.POST.get('opensubtitle') != None):
		choice = request.POST.get('opensubtitle')
		obj = GroupSubtitle.objects.get(whichGroup = group, sSubtitle = choice)
		obj.bShowSubtitle = 'True'
		obj.save()
		subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
		return redirect('/activitygroup/class=%s/%s/discuss' % (gClass, sGroupTitle))

	if (request.method == 'POST' and request.POST.get('deletesubtitle') != None):
		choice = request.POST.get('deletesubtitle')
		obj = GroupSubtitle.objects.get(whichGroup = group, sSubtitle = choice)
		obj.delete()
		subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group, bShowSubtitle = True))
		return redirect('/activitygroup/class=%s/%s/discuss/%s' % (gClass, sGroupTitle, subtitleList[0]))

	return render(request, 'ActivityGroup/activitygroup_discuss.html', locals())

def published(request, gClass = True, sGroupTitle = 1):
	user = request.user
	if (gClass != True and sGroupTitle == 1):
		return redirect('/activitygroup')
	try:
		group = ActivityGroup.objects.get(sGroupTitle = sGroupTitle)
	except:
		return HttpResponse("該活動群組不存在")
	subtitleList = list(GroupSubtitle.objects.filter(whichGroup = group))
	memberList = [user.iUserAccount for user in UserOfActivityGroup.objects.filter(idOfGroup = group)]
	# 取出hashtag
	TAG = list(ActivityGroupOfTopicTag.objects.filter(idOfGroup = group))
	# 取出文章留言
	commentList = list(CommentOfActivityGroup.objects.filter(idOfGroup = group).order_by('CommentTime'))
	commentNumber = len(commentList)
	for com in commentList:
		com.likeCommentNumber = len(UserLikesGroupComment.objects.filter(commentofActivityGroup = com))
		com.save()
	
	# 取出文章讚
	heart = list(UserLikesActivityGroup.objects.filter(idOfGroup = group))
	heartNumber = len(heart)
	ULArticle = list(UserLikesActivityGroup.iAccount for UserLikesActivityGroup in heart)
	# 取出收藏
	CLA = list(UserCollectActivityGroup.objects.filter(idOfGroup = group))
	CLArticle = list(UserCollectActivityGroup.iAccount for UserCollectActivityGroup in CLA)

	userlikescomment = UserLikesGroupComment.objects.filter(iAccount = user)
	userlikescomment = [userlikescomment.commentofActivityGroup for userlikescomment in userlikescomment]

	comment = request.POST.get('comment')
	action = request.GET.get('action')
	warn = request.GET.get('warn')

	if action == 'like':	
		if user not in ULArticle:
			UserLikesActivityGroup.objects.create(iAccount=user,idOfGroup=group)
		else:
			TobeDelete = UserLikesActivityGroup.objects.get(iAccount=user,idOfGroup=group)
			TobeDelete.delete()
		return redirect('/activitygroup/class=True/%s' % (sGroupTitle))

	if action == 'collect':	
		if user not in CLArticle:
			UserCollectActivityGroup.objects.create(iAccount=user,idOfGroup=group)
		else:
			TobeDelete = UserCollectActivityGroup.objects.get(iAccount=user,idOfGroup=group)
			TobeDelete.delete()
		return redirect('/activitygroup/class=True/%s' % (sGroupTitle))

	if request.GET.get('like_comment') != None:
		idOfComment = request.GET.get('like_comment')
		comment = CommentOfActivityGroup.objects.get(pk = idOfComment)
		
		like, isNew = UserLikesGroupComment.objects.get_or_create(commentofActivityGroup = comment, iAccount = user)
		if isNew != True:
			like.delete()
		return redirect('/activitygroup/class=True/%s' % (sGroupTitle))

	if comment:
		CommentOfActivityGroup.objects.create(iAccount=user,sContentOfComment=comment,idOfGroup=group)
		return redirect('/activitygroup/class=True/%s' % (sGroupTitle))

	return render(request, 'ActivityGroup/activitygroup_published.html', locals())