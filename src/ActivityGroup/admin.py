from django.contrib import admin
from ActivityGroup.models import *

class GroupSubtitleInline(admin.TabularInline):
	model = GroupSubtitle
	verbose_name_plural = '子標題'

class GroupThreeMInline(admin.TabularInline):
	model = GroupThreeM
	verbose_name_plural = '便利貼'

class UserOfActivityGroupInline(admin.TabularInline):
	model = UserOfActivityGroup
	verbose_name_plural = '群組成員'

class has_all_permitInline(admin.TabularInline):
	model = has_all_permit
	verbose_name_plural = '活動群組管理員'

class TopicTagInline(admin.TabularInline):
	model = ActivityGroupOfTopicTag
	verbose_name_plural = '話題標籤'

class CollectionsInline(admin.TabularInline):
	model = UserCollectActivityGroup
	verbose_name_plural = '收藏'

class LikesInline(admin.TabularInline):
	model = UserLikesActivityGroup
	verbose_name_plural = '按讚'

class CommentInline(admin.TabularInline):
	model = CommentOfActivityGroup
	verbose_name_plural = '留言'

@admin.register(ActivityGroup)
class ActivityGroupAdmin(admin.ModelAdmin):
	inlines = [
		TopicTagInline,
		GroupSubtitleInline,
		GroupThreeMInline,
		CommentInline,
		UserOfActivityGroupInline,
		has_all_permitInline,
		CollectionsInline,
		LikesInline,
	]

@admin.register(TopicTag)
class TopicTag(admin.ModelAdmin):
	pass


class UserLikesGroupCommentInline(admin.TabularInline):
	model = UserLikesGroupComment
	verbose_name_plural = '按讚留言'

@admin.register(CommentOfActivityGroup)
class CommentAdmin(admin.ModelAdmin):
	inlines = [
		UserLikesGroupCommentInline,
	]