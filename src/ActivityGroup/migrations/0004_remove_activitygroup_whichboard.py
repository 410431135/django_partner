# Generated by Django 2.2.6 on 2019-11-18 05:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ActivityGroup', '0003_activitygroup_ispublished'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='activitygroup',
            name='whichBoard',
        ),
    ]
