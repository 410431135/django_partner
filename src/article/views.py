from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from article.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from ActivityGroup.models import *
from user.models import *
from article.forms import *
import uuid
from django.db.models import Q

def board(request, sBoardName = 'all'):
	user = request.user
	userList = User.objects.all()
	if user in userList:
		user = User.objects.get(username = user)
	boardList = list(Board.objects.all().order_by('-idOfBoard'))
	if (sBoardName == 'all'):
		# 取出所有文章
		articleList = list(Article.objects.filter(bIsInEdtion = False).order_by('-ArticlePostTime'))
		# 取出所有hashtag
		TagList = list(TopicTag.objects.all())
	else:
		# 取出該板塊文章
		uuidOfBoard = Board.objects.get(sBoardName = sBoardName)
		articleList = list(Article.objects.filter(whichBoard = uuidOfBoard, bIsInEdtion = False).order_by('-ArticlePostTime'))
		groupList = list(ActivityGroup.objects.filter(whichBoard = uuidOfBoard).order_by('-createdTime'))
		# 取出板塊文章之hashtag
		TagList =[]
		Topic = []
		for article in articleList:
			if (ArticleOfTopicTag.objects.filter(iSerialNumberOfArticle = article)):
				TagList = list(set(TagList).union(set(list(ArticleOfTopicTag.objects.filter(iSerialNumberOfArticle = article)))))
				for i in TagList:
					Topic.append(i.idOfTopic)
		Topic = list(set(Topic))

	for article in articleList:
		article.likeArticleNumber = len(UserLikesArticle.objects.filter(iSerialNumberOfArticle = article))
		article.commentNumber = len(CommentOfArticle.objects.filter(iSerialNumberOfArticle = article))
		article.save()
				
	return render(request, 'article/article_board.html', locals())

def search(request, sBoardName = 'all'):
	user = request.user
	userList = User.objects.all()
	search = request.GET.get('search')
	boardList = list(Board.objects.all().order_by('-idOfBoard'))
	# 取出標題內文含搜尋文字之文章
	if (sBoardName == 'all'):
		articleList = list(Article.objects.filter(Q(sTitle__contains = search, bIsInEdtion = False)|Q(sContent__contains = search, bIsInEdtion = False)).order_by('-ArticlePostTime'))
	else:
		uuidOfBoard = Board.objects.get(sBoardName = sBoardName)
		articleList = list(Article.objects.filter(Q(whichBoard = uuidOfBoard, sTitle__contains = search, bIsInEdtion = False)|Q(whichBoard = uuidOfBoard, sContent__contains = search, bIsInEdtion = False)).order_by('-ArticlePostTime'))
	pieceOfdata = len(articleList)

	return render(request, 'article/article_board.html', locals())

def hashtag(request, sTopic = 1):
	user = request.user
	userList = User.objects.all()
	boardList = list(Board.objects.all().order_by('-idOfBoard'))
	if (sTopic == 1):
		return redirect('/board')
	try:
		topic = TopicTag.objects.get(sTopic = sTopic)
	except:
		return HttpResponse("該文章或板塊不存在")

	articleList = list(ArticleOfTopicTag.objects.filter(idOfTopic = topic))
	articleList = list(article.iSerialNumberOfArticle for article in articleList)

	return render(request, 'article/article_board.html', locals())

def article_detail(request, sBoardName = 1, iSerialNumberOfArticle = 1):
	user = request.user
	userList = User.objects.all()
	boardList = list(Board.objects.all().order_by('-idOfBoard'))
	if (sBoardName == 1 and iSerialNumberOfArticle == 1):
		return redirect('/board')
	try:
		board = Board.objects.get(sBoardName = sBoardName)
		article = Article.objects.get(iSerialNumberOfArticle = iSerialNumberOfArticle)
	except:
		return HttpResponse("該文章或板塊不存在")

	# 新留言
	comment = request.POST.get('comment')
	action = request.GET.get('action')
	warn = request.GET.get('warn')
	
	author = User.objects.get(username = article.sAuthor)
	# 以下再加入需要的文章作者資訊（UserInfo）
	# authorinfo = {
	# 	'UserName':author.get_full_name(),
	# 	'sNickName':author.userinfo.sNickName,
	# 	'article_number':author.userinfo.iArticleNumber,
	# 	'score':author.userinfo.iScore,
	# 	'read':author.userinfo.iReadingScore,
	# 	'register':author.date_joined.strftime('%Y-%m-%d'),
	# 	'registerDays':(timezone.now() - author.date_joined).days,
	# 	'offlineDays':(timezone.now() - author.last_login).days,
	# 	'intro':author.userinfo.sShortIntroduction,
	# 	}

	# 取出文章留言
	commentList = list(CommentOfArticle.objects.filter(iSerialNumberOfArticle = article).order_by('-CommentTime'))
	commentUser = list(CommentOfArticle.iAccount for CommentOfArticle in commentList)
	commentNumber = len(commentList)
	for com in commentList:
		com.likeCommentNumber = len(UserLikesComment.objects.filter(commentofArticle = com))
		com.save()
	
	# 取出按讚文章的user
	heart = list(UserLikesArticle.objects.filter(iSerialNumberOfArticle = article))
	ULArticle = list(UserLikesArticle.iAccount for UserLikesArticle in heart)
	# 取出收藏文章的user
	CLA = list(UserCollectArticle.objects.filter(iSerialNumberOfArticle = article))
	CLArticle = list(UserCollectArticle.iAccount for UserCollectArticle in CLA)
	# 取出文章之hashtag
	TAG = list(ArticleOfTopicTag.objects.filter(iSerialNumberOfArticle = article))
	
	# 取出按讚的留言
	if user in userList:
		userlikescomment = UserLikesComment.objects.filter(iAccount = user)
		userlikescomment = [userlikescomment.commentofArticle for userlikescomment in userlikescomment]

	# 按讚文章
	if action == 'like':
		like, isNew = UserLikesArticle.objects.get_or_create(iAccount = user,iSerialNumberOfArticle = article)
		if isNew != True:
			like.delete()	
		article.likeArticleNumber = len(UserLikesArticle.objects.filter(iSerialNumberOfArticle = article))
		article.save()
		return redirect('/board/%s/%s' % (sBoardName, iSerialNumberOfArticle))
	# 收藏文章
	if action == 'collect':	
		collection, isNew = UserCollectArticle.objects.get_or_create(iAccount = user,iSerialNumberOfArticle = article)
		if isNew != True:
			collection.delete()
		return redirect('/board/%s/%s' % (sBoardName, iSerialNumberOfArticle))
	# 新增留言
	if comment:
		CommentOfArticle.objects.create(iAccount = user, sContentOfComment = comment, iSerialNumberOfArticle = article)
		article.commentNumber = len(CommentOfArticle.objects.filter(iSerialNumberOfArticle = article))
		article.save()
		return redirect('/board/%s/%s' % (sBoardName, iSerialNumberOfArticle))
	# 按讚留言
	if request.GET.get('like_comment') != None:
		idOfComment = request.GET.get('like_comment')
		comment = CommentOfArticle.objects.get(pk = idOfComment)
		like, isNew = UserLikesComment.objects.get_or_create(commentofArticle = comment, iAccount = user)
		if isNew != True:
			like.delete()
		return redirect('/board/%s/%s' % (sBoardName, iSerialNumberOfArticle))
		
	return render(request, 'article/article_article_detail.html', locals())

@login_required
def new_post(request):
	user = request.user
	userList = User.objects.all()
	article = ''
	# get last edited article if exits one, otherwise create new one.
	if len(Article.objects.filter(bIsInEdtion = True, sAuthor = user)) == 0:
		article = Article.objects.create(bIsInEdtion = True, sAuthor = user, sTitle = '%s的新文章' % (user))
	else:
		article = Article.objects.get(bIsInEdtion = True, sAuthor = user)

	if article.sContent:
		sTitle = article.sTitle
	else:
		sTitle = ''

	data = {
		'sTitle' : sTitle,
		'sContent' : article.sContent,
		'sBoardName' : article.whichBoard,
		'sTag' : [topic.idOfTopic for topic in ArticleOfTopicTag.objects.filter(iSerialNumberOfArticle = article)],
	}

	articleNewPostform = ArticleNewPostForm(initial = data)
	articleList = [article.sTitle.lower() for article in Article.objects.all()]
	articleList.remove(article.sTitle.lower())

	topicList = TopicTag.objects.all()
	topicListName = [topic.sTopic for topic in topicList]
	oldtopicList = [topic.idOfTopic for topic in ArticleOfTopicTag.objects.filter(iSerialNumberOfArticle = article)]
	oldtopicListName = [topic.sTopic for topic in oldtopicList]
	
	action = request.POST.get('action')
	if action:
		choice = request.POST.getlist('select_tag')
		articleNewPostform = ArticleNewPostForm(request.POST, initial = data)
		if articleNewPostform.has_changed():
			articleNewPostform.is_valid()
			sBoardName = articleNewPostform.cleaned_data['sBoardName']
			sTitle = articleNewPostform.cleaned_data['sTitle']
			if (sTitle.lower() in articleList): 
			 	error = '您輸入的內容與現有重複了！'
			 	articleNewPostform = ArticleNewPostForm(request.POST)
			 	return render(request, 'article/article_new_post.html', locals())
			else:
				sContent = articleNewPostform.cleaned_data['sContent']
				Article.objects.filter(bIsInEdtion = True, sAuthor = user).update(sTitle = sTitle, sContent = sContent, whichBoard = sBoardName)

		for i in choice:
			if i not in oldtopicListName:
				if i in topicListName:
					topic = TopicTag.objects.get(sTopic = i)
				else:
					topic = TopicTag.objects.create(sTopic = i)
				ArticleOfTopicTag.objects.create(iSerialNumberOfArticle = article, idOfTopic = topic)
		for i in oldtopicListName:
			if i not in choice:
				topic = TopicTag.objects.get(sTopic = i)
				ArticleOfTopicTag.objects.get(iSerialNumberOfArticle = article, idOfTopic = topic).delete()

		if action == 'send':
			articleNewPostform.is_valid()
			sBoardName = articleNewPostform.cleaned_data['sBoardName']
			sTitle = articleNewPostform.cleaned_data['sTitle']
			Article.objects.filter(bIsInEdtion = True, sAuthor = user).update(bIsInEdtion = False)
			return redirect('/board/%s/%s/' % (sBoardName, iSerialNumberOfArticle))


	# if action == 'send':
	# 	articleNewPostform = ArticleNewPostForm(request.POST)
	# 	if articleNewPostform.has_changed():
	# 		articleNewPostform.is_valid()
	# 		sBoardName = articleNewPostform.cleaned_data['sBoardName']
	# 		sTitle = articleNewPostform.cleaned_data['sTitle']
	# 		if (sTitle in articleList): 
	# 		 	error = '您輸入的內容與現有重複了'
	# 		 	articleNewPostform = ArticleNewPostForm(request.POST)
	# 		 	return render(request, 'article/article_new_post.html', locals())
	# 		sContent = articleNewPostform.cleaned_data['sContent']
	# 		Article.objects.filter(bIsInEdtion = True, sAuthor = user).update(sTitle = sTitle, sContent = sContent, whichBoard = sBoardName, bIsInEdtion = False)

	# 		if (request.POST.get('select_tag') != None):
	# 			choice = request.POST.getlist('select_tag')
	# 			for i in choice:
	# 				if i in topicList:
	# 					topic = TopicTag.objects.get(sTopic = i)
	# 				else:
	# 					topic = TopicTag.objects.create(sTopic = i)
	# 				ArticleOfTopicTag.objects.create(iSerialNumberOfArticle = article, idOfTopic = topic)
	# 	return redirect('/board/%s/%s/' % (sBoardName, sTitle))

	return render(request, 'article/article_new_post.html', locals())
