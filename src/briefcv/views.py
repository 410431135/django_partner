from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from briefcv.models import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from user.models import *
from briefcv.forms import *
import uuid
from django.db.models import Q
# Create your views here.

@login_required
def mybriefcv(request):
	user = request.user
	userList = User.objects.all()
	BCv = BriefCv.objects.filter(sAuthor = user).order_by('BriefCvPostTime')
	if request.method == 'POST' and request.POST.getlist('to_delete') != []:
		to_delete = request.POST.getlist('to_delete')[0]
		#set_mycv_empty(cv, to_delete)
		to_delete = BriefCv.objects.filter(iSerialNumberOfBriefCv = to_delete)
		to_delete.delete()
		return redirect('/briefcv/mycv')

	mycvs = BCv
	return render(request, 'briefcv/usercv_manage.html', locals())

@login_required
def new_mycv(request):
	user = request.user
	userList = User.objects.all()
	newcv = BriefCv.objects.create(sAuthor = request.user,sCvName = '我的履歷')
	newpage = PagesOfBriefCv.objects.create(iSerialNumberOfBriefCv = newcv, iPageNumber = '1-1', iMouldNumber = 1,sTitle ='',sContent ='',sPictureURL ='')
	
	return redirect('/briefcv/mycv')
	#return redirect('../%s' % (newcv.iSerialNumberOfBriefCv))

@login_required
def briefcv_detail(request, iSerialNumberOfBriefCv = 1):
	user = request.user
	userList = User.objects.all()
	if (iSerialNumberOfBriefCv == 1):
		return redirect('/board')
	try:
		BCv = BriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv)
		CvName = BCv.sCvName

		#BCvID = BCv.iSerialNumberOfBriefCv
	except:
		return HttpResponse("該履歷不存在")

	if BCv.sAuthor != user:
		return HttpResponse("沒有權限")


	if (request.method == 'POST' and request.POST.get('pageno') != None):
		briefcv_deletepage(request,iSerialNumberOfBriefCv,999)
		BCv.sCvName = request.POST.getlist('cvName')[0]
		BCv.save()
		return redirect('.')		

	PagesList = list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv).order_by('iPageNumber'))

	if request.method == 'POST' and request.POST.getlist('to_delete') != []:
		to_delete = request.POST.getlist('to_delete')[0]
		#set_mycv_empty(cv, to_delete)
		to_delete = PagesOfBriefCv.objects.filter(idOfPage = to_delete)[0]
		p = briefcv_deletepage(request,iSerialNumberOfBriefCv,to_delete)
		if(p=='..'):
			return redirect('..')
		return redirect('.')

	if request.method == 'POST' and request.POST.get('act') == '新增':
		PagesList = list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber__contains = '-1').order_by('iPageNumber'))
		newpage = PagesOfBriefCv.objects.create(iSerialNumberOfBriefCv = BCv, iPageNumber = f'{len(PagesList)+1}-1', iMouldNumber = 1,sTitle ='',sContent ='',sPictureURL ='')
		return redirect('.')

	Nextpage = PagesList[0].iPageNumber
	PagesNumber = len(PagesList)
	status = 0
#總覽	
	#return render(request, 'briefcv/usercv_mycv%s.html'%(PagesList), locals())
	return render(request, 'briefcv/usercv_mycv.html', locals())

@login_required
def briefcv_pages(request, iSerialNumberOfBriefCv = 1, iPageNumber = 1):
	user = request.user
	userList = User.objects.all()

	BCv = BriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv)
	Page = PagesOfBriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber = iPageNumber)
	Pages = [i.iPageNumber for i in list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv))]
	Pageno = [int(Page.iPageNumber.split('-')[0]),int(Page.iPageNumber.split('-')[1])]
	PagesList = list(ListAndTimeLine.objects.filter(idOfPage = Page,iMouldNumber = Page.iMouldNumber).order_by('iPageNumber'))
	Pagelen=len(list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv).order_by('iPageNumber')))

	if BCv.sAuthor != user:
		return HttpResponse("沒有權限")
	
	sTitle = Page.sTitle
	sContent = Page.sContent
	#Mould = Page.iMouldNumber
	
	if Page.iMouldNumber == 4:
		TL = []
		LTitle = []
		for p in PagesList:
			if (p.iMouldNumber==4):
				w = p.sContent.split('](split/)[')
				TL.append(w[1:])
				LTitle.append(p.sTitle)
	elif Page.iMouldNumber == 5:
		TL = Page.sPictureURL.split('[img/]')
		player = TL[0]
		TL = TL[1:]

	if request.method == 'POST':
		form = request.POST#.getlist('select_tag')
		listl = []
		if (Page.iMouldNumber == 1):
			listl = form.getlist('LContent')
		elif(Page.iMouldNumber == 3):
			listl = form.getlist('listtype')
		elif(Page.iMouldNumber == 4):
			listl = form.getlist('TimelineL')

		if form.getlist('title'):
			sTitle = form.getlist('title')[0]
		if form.getlist('content'):
			sContent = form.getlist('content')[0]
		if listl:
			listt = form.getlist('LTitle')
			listc = form.getlist('LContent')
			lists = form.getlist('LSrc')
		
		PagesList = list(ListAndTimeLine.objects.filter(idOfPage = Page,iMouldNumber = Page.iMouldNumber).order_by('iPageNumber'))
		while not(len(PagesList)==len(listl)):
			if len(PagesList)>len(listl):
				PagesList[-1].delete()
			elif len(PagesList)<len(listl):
				newlist = ListAndTimeLine.objects.create(idOfPage = Page, iPageNumber = len(PagesList)+1 ,sTitle ='',sContent ='',sPictureURL ='',iMouldNumber = Page.iMouldNumber)
			PagesList = list(ListAndTimeLine.objects.filter(idOfPage = Page,iMouldNumber = Page.iMouldNumber).order_by('iPageNumber'))

		textN = 0
		srcN = 0
		TL = []

		for i in range(len(listl)):
			if Page.iMouldNumber == 3:
				if listl[i]=='INPUT':
					ListAndTimeLine.objects.filter(idOfList = PagesList[i].idOfList).update(sPictureURL = '',sTitle = listt[textN], sContent = listc[textN],iMouldNumber = 3)
					textN += 1
				elif (listl[i]=='IMG') or (listl[i]=='IFRAME'):
					ListAndTimeLine.objects.filter(idOfList = PagesList[i].idOfList).update(sTitle = '', sContent = lists[srcN],sPictureURL = listl[i],iMouldNumber=3)
					srcN += 1
					
			
			elif Page.iMouldNumber == 1:
				ListAndTimeLine.objects.filter(idOfList = PagesList[i].idOfList).update(sTitle = listt[i], sContent = listc[i],iMouldNumber=1)
			elif Page.iMouldNumber == 4:
				TL = form.getlist(f'addtopic_{i}')
				w = ''
				for j in TL:
					w = w + '](split/)' + f'{j}'
				ListAndTimeLine.objects.filter(idOfList = PagesList[i].idOfList).update(sTitle = listl[i], sContent = w,iMouldNumber=4)	
					
		if(Page.iMouldNumber == 5):
			listl = form.getlist('img')
			w = f"{form.getlist('player')[0]}"
			for i in listl:
				w = w + '[img/]'+ f"{i}"
			Page.sPictureURL = w
			Page.save()

		PagesOfBriefCv.objects.filter(idOfPage = Page.idOfPage).update(sTitle = sTitle,sContent = sContent)
		#return render(request, 'briefcv/000.html', locals())
		if (request.POST.getlist('leave')):
			return redirect('/briefcv/mycv/')
		else:
			return redirect('.')

	#return render(request, 'briefcv/000.html', locals())
	return render(request, 'briefcv/模板%s.html'%(Page.iMouldNumber), locals())

@login_required
def briefcv_pages_nextmould(request, iSerialNumberOfBriefCv = 1, iPageNumber = 1, nextmould = 1):
	user = request.user
	userList = User.objects.all()

	BCv = BriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv)
	Page = PagesOfBriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber = iPageNumber)
	Pageno = [int(Page.iPageNumber.split('-')[0]),int(Page.iPageNumber.split('-')[1])]

	if BCv.sAuthor != user:
		return HttpResponse("沒有權限")



	if(nextmould == 'clr'):
		newpage = PagesOfBriefCv.objects.create(iSerialNumberOfBriefCv = BCv, iPageNumber = Page.iPageNumber, iMouldNumber = Page.iMouldNumber,sTitle ='',sContent ='',sPictureURL ='')
		Page.delete()

		return redirect('../../%s' % (iPageNumber) )

	elif(nextmould == 'd'):
		iPageNumber = briefcv_deletepage(request,iSerialNumberOfBriefCv,Page)
		#return render(request, 'briefcv/000.html', locals())
		return redirect('../../%s' % (iPageNumber) )

	elif (nextmould == '1') or (nextmould == '-1'):
		Page.iMouldNumber = (Page.iMouldNumber + int(nextmould))%6
		if Page.iMouldNumber == 0:
			Page.iMouldNumber = 6
		Page.save()
		return redirect('../' )

@login_required
def briefcv_newpage(request, iSerialNumberOfBriefCv = 1,iPageNumber = 0):

	user = request.user
	userList = User.objects.all()
	BCv = BriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv)
	if BCv.sAuthor != user:
		return HttpResponse("沒有權限")

	if iPageNumber == '0':
		PagesList = list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber__contains = '-1').order_by('iPageNumber'))
		newpage = PagesOfBriefCv.objects.create(iSerialNumberOfBriefCv = BCv, iPageNumber = f'{len(PagesList)+1}-1', iMouldNumber = 1,sTitle ='',sContent ='',sPictureURL ='')
	elif PagesOfBriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber = iPageNumber):
		Pageno = iPageNumber.split('-')[0]
		PageLayer = list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber__contains = f'{Pageno}-').order_by('iPageNumber'))
		newpage = PagesOfBriefCv.objects.create(iSerialNumberOfBriefCv = BCv, iPageNumber = f'{Pageno}-{len(PageLayer)+1}', iMouldNumber = 1,sTitle ='',sContent ='',sPictureURL ='')
	else:
		return redirect('../../1-1')
	return redirect('../../%s' % (newpage.iPageNumber))

@login_required
def briefcv_deletepage(request,iSerialNumberOfBriefCv,Page):
	user = request.user
	userList = User.objects.all()
	BCv = BriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv)
	if BCv.sAuthor != user:
		return HttpResponse("沒有權限")


	N = len( list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber__contains = '-1')) )
	if Page != 999:
		Pageno = [int(Page.iPageNumber.split('-')[0]),int(Page.iPageNumber.split('-')[1])]
		iPageNumber = Page.iPageNumber
		Page.delete()

	n = 1
	i = 1
	while (n<=N):
		M = len( list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber__contains = f'{n}-')) )
		m = 1
		j = 1
		while (m<=M+1):
			P = list( PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber__contains = f'{n}-{m}') )
			if P!=[]:
				P[0].iPageNumber = f'{i}-{j}'
				P[0].save()
				j+=1
			m+=1
		if (M!=0):
			i+=1
		n+=1

	if Page == 999:
		return 0

	if ( list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv))==[] ):
		BriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv).delete()
		return '..'
	#return render(request, 'briefcv/000.html', locals())
	while (list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber = iPageNumber)) == []):
		iPageNumber = f'{Pageno[0]-1}-1' if (Pageno[1]==1) else (f'{Pageno[0]}-{Pageno[1]-1}')
	return iPageNumber
	



		
		


def briefcv_output(request, iSerialNumberOfBriefCv = 1, iPageNumber = 1):
	user = request.user
	userList = User.objects.all()
	
	BCv = BriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv)
	Page = PagesOfBriefCv.objects.get(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv,iPageNumber = iPageNumber)
	Pages = [i.iPageNumber for i in list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv))]
	Pageno = [int(Page.iPageNumber.split('-')[0]),int(Page.iPageNumber.split('-')[1])]
	PagesList = list(ListAndTimeLine.objects.filter(idOfPage = Page,iMouldNumber = Page.iMouldNumber).order_by('iPageNumber'))
	Pagelen=len(list(PagesOfBriefCv.objects.filter(iSerialNumberOfBriefCv = iSerialNumberOfBriefCv).order_by('iPageNumber')))

	
	sTitle = Page.sTitle
	sContent = Page.sContent
	#Mould = Page.iMouldNumber
	
	if Page.iMouldNumber == 4:
		TL = []
		LTitle = []
		for p in PagesList:
			if (p.iMouldNumber==4):
				w = p.sContent.split('](split/)[')
				TL.append(w[1:])
				LTitle.append(p.sTitle)
	elif Page.iMouldNumber == 5:
		TL = Page.sPictureURL.split('[img/]')
		player = TL[0]
		TL = TL[1:]

	

	#return render(request, 'briefcv/000.html', locals())
	return render(request, 'briefcv/D模板%s.html'%(Page.iMouldNumber), locals())