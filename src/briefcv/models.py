from django.db import models
import uuid
from django.utils import timezone
from django.contrib.auth.models import User
# Create your models here.
#履歷資訊
class BriefCv(models.Model):
	sAuthor = models.ForeignKey(User, verbose_name = '作者', on_delete = models.CASCADE, null = True)
	iSerialNumberOfBriefCv = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
	sCvName = models.CharField(verbose_name = '履歷名稱', max_length = 128)
	sContent = models.TextField(verbose_name = '文章內容', blank = True)
	# 實作時應存當時時間
	BriefCvPostTime = models.DateTimeField(verbose_name = '創建時間', default = timezone.now)
	# 是否在編輯中(未送出)
	bIsInEdtion = models.BooleanField(default = True)
	
	def __str__(self):
		return '%s by %s' % (self.sCvName, self.sAuthor.get_full_name())

#履歷內頁
class PagesOfBriefCv(models.Model):
	iSerialNumberOfBriefCv = models.ForeignKey(BriefCv, on_delete = models.CASCADE)
	idOfPage = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
	iPageNumber = models.CharField(verbose_name = "頁碼", max_length = 16, null = True)
	iMouldNumber = models.PositiveIntegerField(verbose_name = "模板類型", null = True)
	sTitle = models.CharField(verbose_name = '頁面標題', max_length = 128,  blank = True, null = True)
	sContent = models.TextField(verbose_name = '頁面內文(156)', blank = True, null = True)
	sPictureURL = models.TextField(verbose_name = '圖片嵌入連結(5)', blank = True, null = True)

class ListAndTimeLine(models.Model):
	idOfPage = models.ForeignKey(PagesOfBriefCv, on_delete = models.CASCADE)
	idOfList = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
	iPageNumber = models.PositiveIntegerField(verbose_name = "頁碼", null = True)
	iMouldNumber = models.PositiveIntegerField(verbose_name = "模板類型(34)", null = True)
	sTitle = models.CharField(verbose_name = '列表標題或時間點', max_length = 128, blank = True, null = True)
	sContent = models.TextField(verbose_name = '文字內容', blank = True, null = True)
	sPictureURL = models.TextField(verbose_name = '圖片嵌入連結', blank = True, null = True)