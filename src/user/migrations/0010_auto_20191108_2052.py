# Generated by Django 2.2.6 on 2019-11-08 12:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0009_auto_20191108_1807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userinfo',
            name='iReadingScore',
            field=models.IntegerField(blank=True, null=True, verbose_name='閱讀權限'),
        ),
        migrations.AlterField(
            model_name='userinfo',
            name='iRegisterDays',
            field=models.IntegerField(blank=True, null=True, verbose_name='註冊天數'),
        ),
        migrations.AlterField(
            model_name='userinfo',
            name='iScore',
            field=models.IntegerField(blank=True, null=True, verbose_name='積分'),
        ),
        migrations.AlterField(
            model_name='userinfo',
            name='imageAvatar',
            field=models.ImageField(blank=True, default='', upload_to='avatar/'),
        ),
    ]
