from django import forms
from user.models import *
from datetime import datetime
#from captcha.fields import CaptchaField

class RegisterForm(forms.Form):
	DEPARTMENT_CHOICES = (
		(None, '系所'),
		('1', '國文學系'),
		('2', '英語學系'),
		('3', '地理學系'),
		('4', '語言與文化學士原住民專班'),
		('5', '數學系數學組'),
		('6', '數學系應用數學組'),
		('7', '化學系'),
		('8', '物理學系'),
		('9', '生物科技系'),
		('10', '教育學系'),
		('11', '特殊教育學系'),
		('12', '體育學系'),
		('13', '事業經營學系'),
		('14', '運動競技與產業學士原住民專班'),
		('15', '工業科技教育學系科技教育與訓練組'),
		('16', '工業科技教育學系能源與冷凍空調組'),
		('17', '工業設計學系'),
		('18', '光電與通訊工程學系'),
		('19', '電子工程學系'),
		('20', '軟體工程與管理學系'),
		('21', '電機工程學系'),
		('22', '美術學系'),
		('23', '音樂學系'),
		('24', '視覺設計學系'),
		('25', '藝術產業學士原住民專班'),
	)
	GENDER_CHOICES = (
		(None, '性別'),
		('1', '男'),
		('2', '女'),
		('3', '其他'),
	)
	username = forms.CharField(label = "帳號設定", max_length = 128, widget = forms.TextInput(attrs={'id' : 'account','placeholder':'帳號設定'}))
	password1 = forms.CharField(label = "密碼(大於8位)", max_length = 256, widget = forms.PasswordInput(attrs={'id' : 'password1','placeholder':'密碼設定'}))
	password2 = forms.CharField(label = "確認密碼", max_length = 256, widget = forms.PasswordInput(attrs={'id' : 'password2','placeholder':'密碼確認'}))
	name = forms.CharField(label = "姓名", max_length = 16, widget = forms.TextInput(attrs={'id' : 'name','placeholder':'姓名'}))
	department = forms.ChoiceField(label = '系所', choices = DEPARTMENT_CHOICES, widget = forms.Select(attrs={'id': 'department'}))
	email = forms.EmailField(label = "電子信箱", max_length = 256, widget = forms.EmailInput(attrs={'id' : 'mail','placeholder':'電子信箱'}))
	phone = forms.CharField(label = "手機號碼", max_length = 16, widget = forms.TextInput(attrs={'id' : 'phone','placeholder':'手機號碼'}),required = False)
	birthday = forms.DateField(widget = forms.widgets.DateInput(attrs={'type': 'date', 'id': 'birthday'}))
	gender = forms.ChoiceField(label = '性別', choices = GENDER_CHOICES, widget = forms.Select(attrs={'id': 'gender'}))
	# captcha = CaptchaField(error_messages={'invalid': u'驗證碼輸入錯誤'})

class LoginForm(forms.Form):
	username = forms.CharField(label = "帳號", max_length = 128, widget = forms.TextInput(attrs={'id' : 'account','placeholder':'帳號'}))
	password = forms.CharField(label = "密碼", max_length = 256, widget = forms.PasswordInput(attrs={'id' : 'password','placeholder':'密碼'}))

class UploadImageForm(forms.ModelForm):
    class Meta:
        model = UserInfo
        fields = ['imageAvatar']
		