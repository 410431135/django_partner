from django.db import models
from django.contrib.auth.models import User
import os, uuid

def user_directory_path(instance, filename):
	ext = filename.split('.')[-1]
	filename = '{}.{}'.format(uuid.uuid4().hex, ext)
	return os.path.join('avatar', filename)

class UserInfo(models.Model):
	DEPARTMENT_CHOICES = (
		('1', '國文學系'),
		('2', '英語學系'),
		('3', '地理學系'),
		('4', '語言與文化學士原住民專班'),
		('5', '數學系數學組'),
		('6', '數學系應用數學組'),
		('7', '化學系'),
		('8', '物理學系'),
		('9', '生物科技系'),
		('10', '教育學系'),
		('11', '特殊教育學系'),
		('12', '體育學系'),
		('13', '事業經營學系'),
		('14', '運動競技與產業學士原住民專班'),
		('15', '工業科技教育學系科技教育與訓練組'),
		('16', '工業科技教育學系能源與冷凍空調組'),
		('17', '工業設計學系'),
		('18', '光電與通訊工程學系'),
		('19', '電子工程學系'),
		('20', '軟體工程與管理學系'),
		('21', '電機工程學系'),
		('22', '美術學系'),
		('23', '音樂學系'),
		('24', '視覺設計學系'),
		('25', '藝術產業學士原住民專班'),
	)
	GENDER_CHOICES = (
		('1', '男'),
		('2', '女'),
		('3', '其他'),
	)
	iAccount = models.OneToOneField(User, unique = True, verbose_name = '學號', on_delete = models.CASCADE)
	sNickName = models.CharField(max_length = 16, verbose_name = "暱稱", blank = True)
	iArticleNumber = models.PositiveIntegerField(verbose_name = "文章數", blank = True, null = True)	# from 1 to 2,147,483,647
	sShortIntroduction = models.TextField(verbose_name = "短自介", blank = True)
	iScore = models.IntegerField(verbose_name = "積分", null = True, blank = True)
	iReadingScore = models.IntegerField(verbose_name = "閱讀權限", null = True, blank = True)
	# registerDate = models.DateTimeField(verbose_name = "註冊日期", null = True)
	iRegisterDays = models.IntegerField(verbose_name = "註冊天數", null = True, blank = True)
	# iMissingDays = models.IntegerField(verbose_name = "未上線天數", null = True)
	imageAvatar = models.ImageField(upload_to = user_directory_path, default = '', blank = True)
	iDepartment = models.CharField(max_length = 16, verbose_name = "系所", choices = DEPARTMENT_CHOICES, blank = True, null = True)
	iPhone = models.CharField(verbose_name = '電話', max_length = 16, blank = True, null = True)
	iBirthday = models.DateField(verbose_name = '生日', null = True)
	iGender = models.CharField(verbose_name = '性別', max_length = 3, choices = GENDER_CHOICES, blank = True, null = True)

	bShowMyArts = models.BooleanField(verbose_name = '顯示文章', default = True, blank = True, null = True)
	bShowMyCollects = models.BooleanField(verbose_name = '顯示收藏', default = True, blank = True, null = True)
	bShowMyGroups = models.BooleanField(verbose_name = '顯示活動群組', default = True, blank = True, null = True)

	def to_dict(self):
		return {
			'iAccount' : self.iAccount,
			'sNickName': self.sNickName,
			'iArticleNumber': self.iArticleNumber,
			'sShortIntroduction': self.sShortIntroduction,
			'iScore': self.iScore,
			'iReadingScore': self.iReadingScore, 
			# 'registerDate': self.registerDate,
			#'iRegisterDays': self.iRegisterDays,
			# 'iMissingDays': self.iMissingDays,
			'UserName':self.get_full_name(),
			'registerDate':self.date_joined.strftime('%Y-%m-%d'),
			'registerDays':(timezone.now() - self.date_joined).days,
			'offlineDays':(timezone.now() - self.last_login).days,
			'sShortIntroduction':self.sShortIntroduction,
			'iDepartment_grade':self.iDepartment_grade,
			'iPhone':self.iPhone,
			'iBirthday':self.iBirthday,
			'iGender':self.iGender,
		}