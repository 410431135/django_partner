from django import forms
from django.core import validators

class UploadForm(forms.Form):
	file = forms.FileField(label = '檔案上傳', help_text = '必須上傳 .csv 檔案', validators = [validators.FileExtensionValidator(['csv', ])], widget=forms.ClearableFileInput(attrs={'multiple': True}))
	