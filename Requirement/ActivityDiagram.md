# Activity Diagram

# Change log

1. Creating the file. 2021/2/22 LKY
2. Editing the file. 2021/4/06 Han

# Diagram
```uml
@startuml
start
skinparam ConditionEndStyle hline

repeat :主頁;
	if(功能)then(新增/編輯履歷)

	if(新增/編輯履歷介面)then(新增)
	   :新增一份履歷;
	else(編輯)
	   :選擇要編輯的履歷;
	endif


	:編輯履歷內容;
	split
		->//模板1//;
		:編輯標題/內文/聯絡資訊;
	split again
		->//模板2//;
		:編輯標題/內文/圖片;
	split again
		->//模板3//;
		:設定表格;
		:編輯表格內容;
	split again
		->//模板4//;
		:設定時間軸;
		:編輯標題/內文;
	end split


	:儲存履歷;

	if(輸出履歷)then(上傳)
	       :設定標籤;
	       :設定履歷隱私;
	else(列印)
		:列印履歷;
	endif

else(瀏覽履歷)
	:瀏覽履歷介面;
	:搜尋履歷;
	:瀏覽履歷;

endif


repeat while (more action?)
stop
@enduml
```
