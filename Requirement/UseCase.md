# Use Case Diagram

# Change log

1. Creating the file. 2021/2/22 LKY
2. Editing the file. 2021/4/06 Han

# Diagram
```uml
@startuml

:USER: as p0
:師資生: as p1
:manager:
:訪客: as v0

rectangle 履歷系統 {

(瀏覽履歷) as s2

(管理個人履歷) as s1

(編輯履歷) as a2
(設定隱私) as a4
(輸出履歷) as a3

p0 -- s2

s1 <... a2
s1 <... a4
s1 <... a3
v0 -|> p0
p0 <|- p1



:manager: --(審查履歷)


note bottom of (審查履歷):管理已公開的履歷

p1 - a3
p1 -- s1
p1 - a2
p1 - a4


note "分為文本及簡報2種形式" as n1
s2 ... n1
n1 . a3

note right of a4:設定公開/不公開

@enduml
```
