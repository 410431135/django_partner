# Gitbook

參考以下網址進行設置：

https://wcc723.github.io/design/2014/09/04/gitbook-install/

https://www.npmjs.com/package/gitbook

設置好之後只要在寫好時

`gitbook build`

（`gitbook serve` 此指令可以在本地 http://127.0.0.1:4000 看到）

然後上傳至gitlab即可！
