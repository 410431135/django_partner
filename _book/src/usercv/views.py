from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from usercv.models import *
from usercv.cvForm import TotalCVForm
from django.contrib.auth.models import User
import datetime

def get_data(user):
	cv = user.totalcv
	# 這邊可以調整出現的順序
	data = {
		# 其他不在cv的履歷資料
		# '姓名' : user.get_full_name(),

		'姓名' : cv.sName,
		'暱稱' : cv.sNickname,
		'性別' : cv.get_sex_display,
		'生日' : cv.dateBirthday,
		'電話號碼' : cv.sPhoneNumber1,
		'家裡住址' : cv.sAddressHome,
		'電子郵件' : cv.sEmail,
		'中文自傳' : cv.sAutobiography_zh,
		'英文自傳' : cv.sAutobiography_en,
	}
	return data


def set_mycv_default(cv, cvid, length):
	status = ''
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
	for i in range(length):
		status += '1'
	if ( cvid == '1' and cv.sMyCV_1 == None ):
		cv.sMyCV_1 = '1'+'!'+'我的第1份履歷'+'!'+status+'!'+now
	elif ( cvid == '2' and cv.sMyCV_2 == None ):
		cv.sMyCV_2 = '2'+'!'+'我的第2份履歷'+'!'+status+'!'+now
	elif ( cvid == '3' and cv.sMyCV_3 == None ):
		cv.sMyCV_3 = '3'+'!'+'我的第3份履歷'+'!'+status+'!'+now
	elif ( cvid == '4' and cv.sMyCV_4 == None ):
		cv.sMyCV_4 = '4'+'!'+'我的第4份履歷'+'!'+status+'!'+now
	elif ( cvid == '5' and cv.sMyCV_5 == None ):
		cv.sMyCV_5 = '5'+'!'+'我的第5份履歷'+'!'+status+'!'+now
	cv.save()	

def get_mycv(cv, cvid):
	# return a tuple cvName, cvStatus
	if ( cv.sMyCV_1 != None and cvid == cv.sMyCV_1.split('!')[0] ):
		return (cv.sMyCV_1.split('!')[1], cv.sMyCV_1.split('!')[2])
	elif ( cv.sMyCV_2 != None and cvid == cv.sMyCV_2.split('!')[0] ):
		return (cv.sMyCV_2.split('!')[1], cv.sMyCV_2.split('!')[2])
	elif ( cv.sMyCV_3 != None and cvid == cv.sMyCV_3.split('!')[0] ):
		return (cv.sMyCV_3.split('!')[1], cv.sMyCV_3.split('!')[2])
	elif ( cv.sMyCV_4 != None and cvid == cv.sMyCV_4.split('!')[0] ):
		return (cv.sMyCV_4.split('!')[1], cv.sMyCV_4.split('!')[2])
	elif ( cv.sMyCV_5 != None and cvid == cv.sMyCV_5.split('!')[0] ):
		return (cv.sMyCV_5.split('!')[1], cv.sMyCV_5.split('!')[2])
	return ValueError()

def set_mycv(cv, cvid, cvName, cvStatus):
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
	if ( cvid == cv.sMyCV_1.split('!')[0] ):
		cv.sMyCV_1 = '1'+'!'+cvName+'!'+cvStatus+'!'+now
	elif ( cvid == cv.sMyCV_2.split('!')[0] ):
		cv.sMyCV_2 = '2'+'!'+cvName+'!'+cvStatus+'!'+now
	elif ( cvid == cv.sMyCV_3.split('!')[0] ):
		cv.sMyCV_3 = '3'+'!'+cvName+'!'+cvStatus+'!'+now
	elif ( cvid == cv.sMyCV_4.split('!')[0] ):
		cv.sMyCV_4 = '4'+'!'+cvName+'!'+cvStatus+'!'+now
	elif ( cvid == cv.sMyCV_5.split('!')[0] ):
		cv.sMyCV_5 = '5'+'!'+cvName+'!'+cvStatus+'!'+now
	cv.save()

def set_mycv_empty(cv, deleteList):
	if (cv.sMyCV_1 != None and cv.sMyCV_1.split('!')[0] in deleteList):
		cv.sMyCV_1 = None
	if (cv.sMyCV_2 != None and cv.sMyCV_2.split('!')[0] in deleteList):
		cv.sMyCV_2 = None
	if (cv.sMyCV_3 != None and cv.sMyCV_3.split('!')[0] in deleteList):
		cv.sMyCV_3 = None
	if (cv.sMyCV_4 != None and cv.sMyCV_4.split('!')[0] in deleteList):
		cv.sMyCV_4 = None
	if (cv.sMyCV_5 != None and cv.sMyCV_5.split('!')[0] in deleteList):
		cv.sMyCV_5 = None
	cv.save()

@login_required
def manage(request):
	user = request.user
	userList = User.objects.all()
	# return HttpResponse("履歷管理頁面，從這邊可以進入履歷總管、特定履歷、新增履歷")
	new, old = TotalCV.objects.get_or_create(iAccount = user)
	cv = (new or old)
	mycvs = {
		'mycv1':cv.sMyCV_1, 
		'mycv2':cv.sMyCV_2, 
		'mycv3':cv.sMyCV_3, 
		'mycv4':cv.sMyCV_4, 
		'mycv5':cv.sMyCV_5,
	}
	# 若sMyCv_<id> 不為None，則 字典值改為tuple (cvid, cvName)
	for key in mycvs:
		if (mycvs[key] != None):
			temp = mycvs[key].split('!')
			mycvs[key] = list([temp[0], temp[1], temp[3]])

	if request.method == 'POST' and request.POST.getlist('to_delete') != []:
		to_delete = request.POST.getlist('to_delete')
		set_mycv_empty(cv, to_delete)
		return redirect('/cv')

	public = cv.sPublic
	if request.method == 'POST' and request.POST.get('public') != None:
		cv.sPublic = request.POST.get('public')
		cv.save()
		return redirect('/cv')

	return render(request, 'usercv/usercv_manage.html', locals())

@login_required
def new_mycv(request):
	cv = request.user.totalcv
	if cv.sMyCV_1 == None:
		return redirect('/cv/mycv/1/edit')
	elif cv.sMyCV_2 == None:
		return redirect('/cv/mycv/2/edit')
	elif cv.sMyCV_3 == None:
		return redirect('/cv/mycv/3/edit')
	elif cv.sMyCV_4 == None:
		return redirect('/cv/mycv/4/edit')
	elif cv.sMyCV_5 == None:
		return redirect('/cv/mycv/5/edit')
	else:
		return HttpResponse('履歷已達上限！')


@login_required
def totalcv(request):
	user = request.user
	userList = User.objects.all()
	new, old = TotalCV.objects.get_or_create(iAccount = user)
	cv = (new or old)
	# 要跟 get_data 同步處理
	data = {
		# 其他不在cv的履歷資料
		# 'name' : user.get_full_name(),

		'sName' : cv.sName,
		'sNickname' : cv.sNickname,
		'sex' : cv.sex,
		'dateBirthday' : cv.dateBirthday,
		'sPhoneNumber1' : cv.sPhoneNumber1,
		'sAddressHome' : cv.sAddressHome,
		'sEmail' : cv.sEmail,
		'sAutobiography_zh' : cv.sAutobiography_zh,
		'sAutobiography_en' : cv.sAutobiography_en,

		
	}
	# data = TotalCV.objects.filter(iAccount = user).values()[0]
	form = TotalCVForm(initial = data)
	if request.method == 'POST':
		form = TotalCVForm(request.POST, initial = data)
		if form.has_changed():
			form.is_valid()
			TotalCV.objects.filter(iAccount = user).update(**form.cleaned_data)

	return render(request, 'usercv/usercv_totalcv.html', locals())

@login_required
def mycv_edit(request, cvid = '1'):
	if (cvid not in ['1','2','3','4','5']):
		return HttpResponse("87哦輸入正確的數字啦")

	user = request.user
	userList = User.objects.all()
	cv = user.totalcv
	# 獲取履歷欄位 return dict
	data = get_data(user)

	# 設定履歷狀態，若無，則預設
	# sMyCV : '<number>!<name>!<status>' 例如： '1!家教!101001' 
	set_mycv_default(cv, cvid, len(data))
	cvName, cvStatus = get_mycv(cv, cvid)

	if (request.method == 'POST' and request.POST.get('cvName') != None):
		cvName = request.POST.get('cvName')
		set_mycv(cv, cvid, cvName, cvStatus)

	if request.method == 'POST':
		cvStatus = ''
		for key in data:
			cvStatus += request.POST.get(key)
		set_mycv(cv, cvid, cvName, cvStatus)
		if request.method == 'POST' and request.POST.get('act') == '預覽':
			return redirect('/cv/mycv/%s/browse' % cvid)
	mix = []
	i = 0
	for key in data:
		mix.append( ([(key, data[key])], cvStatus[i] )) 
		i += 1	

	


	return render(request, 'usercv/usercv_mycv.html', locals())

@login_required
def mycv_browse(request, cvid = '1'):
	if (cvid not in ['1','2','3','4','5']):
		return HttpResponse("87哦輸入正確的數字啦")
	user = request.user
	userList = User.objects.all()
	cv = user.totalcv
	data = get_data(user)
	

	try:
		cvName, cvStatus = get_mycv(cv, cvid)
	except:
		return HttpResponse("87哦還沒設定看屁看")

	i = 0
	for key in data:
		if cvStatus[i] == '1':
			i+=1
			continue
		else:
			i+=1
			data[key] = ''
		
	return render(request, 'usercv/usercv_mycv_browse.html', locals())

@login_required
def mycv_output(request, cvid = '1'):
	if (cvid not in ['1','2','3','4','5']):
		return HttpResponse("87哦輸入正確的數字啦")
	user = request.user
	userList = User.objects.all()
	cv = user.totalcv
	data = get_data(user)
	

	try:
		cvName, cvStatus = get_mycv(cv, cvid)
	except:
		return HttpResponse("87哦還沒設定看屁看")

	i = 0
	for key in data:
		if cvStatus[i] == '1':
			i+=1
			continue
		else:
			i+=1
			data[key] = ''
		
	return render(request, 'usercv/usercv_mycv_output.html', locals())


def public(request):
	user = request.user
	userList = User.objects.all()
	cvList = TotalCV.objects.all()
		
	return render(request, 'usercv/usercv_public.html', locals())

def public_cv(request,username = '1', cvid = '1'):
	if (cvid not in ['1','2','3','4','5']):
		return HttpResponse("87哦輸入正確的數字啦")
	user = request.user
	userList = User.objects.all()

	cvOwner = User.objects.get(username = username)
	cv = TotalCV.objects.get(iAccount = cvOwner)
	data = get_data(cvOwner)
	

	try:
		cvName, cvStatus = get_mycv(cv, cvid)
	except:
		return HttpResponse("87哦還沒設定看屁看")

	i = 0
	for key in data:
		if cvStatus[i] == '1':
			i+=1
			continue
		else:
			i+=1
			data[key] = ''
		
	return render(request, 'usercv/usercv_public_cv.html', locals())





