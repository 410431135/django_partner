from django.db import models
from django.contrib.auth.models import User
import uuid

class TotalCV(models.Model):
	MALE = 'M'
	FEMALE = 'F'
	OTHERS = 'O'
	NOPROVIDE = 'N'
	sexChoice = [
		(MALE, '男性'),
		(FEMALE, '女性'),
		(OTHERS, '其他'),
		(NOPROVIDE, '不提供')
	]
	TotalCV_ID = models.CharField(max_length = 36, primary_key = True, default = uuid.uuid4, editable = False)
	iAccount = models.OneToOneField(User, unique = True, verbose_name = '使用者學號', on_delete = models.CASCADE)
	sName = models.CharField(verbose_name = '名字', max_length = 36, blank = True)
	sNickname = models.CharField(verbose_name = '暱稱', max_length = 128, blank = True)
	sex = models.CharField(verbose_name = '性別', max_length = 2, choices = sexChoice, blank = True, default = NOPROVIDE)
	sIdentification = models.TextField(verbose_name = '身份', blank = True)
	sPhoneNumber1 = models.CharField(verbose_name = '電話號碼', max_length = 32, blank = True)
	sAddressHome = models.TextField(verbose_name = '家裡住址', blank = True)
	sEmail = models.EmailField(verbose_name = '電子信箱', blank = True)
	dateBirthday = models.DateField(verbose_name = '生日', auto_now = False, null = True, blank = True)
	sAutobiography_zh = models.TextField(verbose_name = '中文自傳', blank = True)
	sAutobiography_en = models.TextField(verbose_name = '英文自傳', blank = True)

	sMyCV_1 = models.CharField(verbose_name = '我的第一份履歷', max_length = 128, blank = True, null = True)
	sMyCV_2 = models.CharField(verbose_name = '我的第二份履歷', max_length = 128, blank = True, null = True)
	sMyCV_3 = models.CharField(verbose_name = '我的第三份履歷', max_length = 128, blank = True, null = True)
	sMyCV_4 = models.CharField(verbose_name = '我的第四份履歷', max_length = 128, blank = True, null = True)
	sMyCV_5 = models.CharField(verbose_name = '我的第五份履歷', max_length = 128, blank = True, null = True)

	sPublic = models.CharField(verbose_name = '公開於論壇', max_length = 1, blank = True, null = True, default = '1')

	def __str__(self):
		return '%s 的總履歷' % self.iAccount.username

