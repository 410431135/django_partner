from django import forms
from article.models import *
from django.forms import CharField, ModelMultipleChoiceField, ModelChoiceField

class ArticleNewPostForm(forms.Form):
	sBoardName = forms.ModelChoiceField(label = '發文版塊', queryset = Board.objects.all().order_by('-idOfBoard'), widget = forms.Select(attrs={'id':'selectboard' }))
	sTitle = forms.CharField(label = '標題', widget = forms.TextInput(attrs={'id' : 'title','placeholder':'標題'}))
	sContent = forms.CharField(label = '內文', widget = forms.Textarea(attrs={'class' : 'text','placeholder':'內文'}))
