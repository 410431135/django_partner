from django.contrib import admin
from briefcv.models import *
# Register your models here.

class BriefCvInline(admin.TabularInline):
	model = BriefCv
	verbose_name_plural = '履歷'


class PagesInline(admin.TabularInline):
	model = PagesOfBriefCv
	verbose_name_plural = 'Page'


class ListTagInline(admin.TabularInline):
	model = ListAndTimeLine
	verbose_name_plural = 'List'


@admin.register(BriefCv)
class BriefCvAdmin(admin.ModelAdmin):
	inlines = [
		PagesInline,
	]

@admin.register(PagesOfBriefCv)
class BriefCvAdmin(admin.ModelAdmin):
	inlines = [
		ListTagInline,
	]	