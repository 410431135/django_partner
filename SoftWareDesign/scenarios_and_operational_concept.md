# Scenarios and Operational Concept

# Change log

1. Creating the file. 2021/04/06 Han
2. rearranged the file so that gitlab can read it properly. 2021/04/06 by Lin.
3. 調整表格內容並與循序圖同步. 2021/4/11 Han

# 編輯/新增履歷

<table>
    <tr>
        <td align="center" colspan="2">參與者：使用者</td>
    </tr>
    <tr>
        <td align="center" colspan="2">目標：編輯/新增履歷</td>
    </tr>
    <tr>
        <td align="center" colspan="2">前置條件：登入帳號</td>
    </tr>
    <tr><td colspan="2"></td></tr>


    <tr>
        <th align="center" colspan="2">主情節</td>
    </tr>
    <tr>
        <th align="center">參與者動作</th>
        <th align="center">系統回應</th>
    </tr>
    <tr>
        <td>1.	選擇編輯/新增履歷
        <br>2.	選擇編輯履歷
        <br>3.	選擇頁面
        <br>4.	選擇模板
        <br>6.	編輯內文
        <br>8.	儲存履歷
        </td>
        <td><br>
        <br>3.	開啟履歷檔案
        <br>
        <br>5.	載入模板格式
        <br>7.  預覽編輯內容
        <br>9.	更新資料庫資料
        <br>10.	回傳成功
        </td>
    </tr>

    <tr>
        <th align="center" colspan="2">替代情節</td>
    </tr>
    <tr>
        <th align="center">參與者動作</th>
        <th align="center">系統回應</th>
    </tr>
    <tr>
        <td>2a.	選擇新增履歷<p><br></td>
        <td>3a.	生成新履歷(回到1)<p><br></td>
    </tr>

    <tr><td colspan="2"></td></tr>
    <tr>
        <td align="center" colspan="2">後置條件：顯示儲存成功</td>
    </tr>
    <tr>
        <td align="center" colspan="2">特別需求：無</td>
    </tr>

</table>



# 輸出履歷

<table>
    <tr>
        <td align="center" colspan="2">參與者：使用者</td>
    </tr>
    <tr>
        <td align="center" colspan="2">目標：輸出履歷</td>
    </tr>
    <tr>
        <td align="center" colspan="2">前置條件：選擇輸出履歷</td>
    </tr>
    <tr><td colspan="2"></td></tr>


    <tr>
        <th align="center" colspan="2">主情節</td>
    </tr>
    <tr>
        <th align="center">參與者動作</th>
        <th align="center">系統回應</th>
    </tr>
    <tr>
        <td>1.  選擇上傳履歷
        <br>3.  設定標籤及履歷隱私
        <br>4.  儲存設定並上傳
        </td>
        <td><br>
        <br>2.  載入設定頁面
        <br>
        <br>5.  匯入資料庫
        <br>6.  更新資料庫資料
        <br>7.  顯示履歷連結
        </td>
    </tr>

    <tr>
        <th align="center" colspan="2">替代情節</td>
    </tr>
    <tr>
        <th align="center">參與者動作</th>
        <th align="center">系統回應</th>
    </tr>
    <tr>
        <td>1a. 選擇列印履歷
        <br>3a. 設定列印資訊
        <p>
        </td>
        <td><br><br>
        <br>5a. 載入需列印內容
        <br>6a. 儲存或列印履歷
        <br>7a. 回傳列印成功<p></td>
    </tr>

    <tr><td colspan="2"></td></tr>
    <tr>
        <td align="center" colspan="2">後置條件：顯示連結/顯示列印成功</td>
    </tr>
    <tr>
        <td align="center" colspan="2">特別需求：無</td>
    </tr>

</table>



# 瀏覽履歷

<table>
    <tr>
        <td align="center" colspan="2">參與者：使用者</td>
    </tr>
    <tr>
        <td align="center" colspan="2">目標：瀏覽履歷</td>
    </tr>
    <tr>
        <td align="center" colspan="2">前置條件：進入瀏覽畫面</td>
    </tr>
    <tr><td colspan="2"></td></tr>


    <tr>
        <th align="center" colspan="2">主情節</td>
    </tr>
    <tr>
        <th align="center">參與者動作</th>
        <th align="center">系統回應</th>
    </tr>
    <tr>
        <td>2.  輸入關鍵字搜尋履歷
        <br>4. 點擊履歷連結
        <br>6. 選擇是否加入書籤
        <p>
        </td>
        <td>1.  顯示列表
        <br>3.  根據關鍵字顯示搜尋結果
        <br>5.  開啟履歷頁面
        <br>7.  更新資料庫
        </td>
    </tr>

    <tr>
        <th align="center" colspan="2">替代情節</td>
    </tr>
    <tr>
        <th align="center">參與者動作</th>
        <th align="center">系統回應</th>
    </tr>
    <tr>
    <td>2a. 選擇書籤列表<p></td>
    <td>3a. 回傳書籤列表<p></td>
    </tr>

    <tr><td colspan="2"></td></tr>
    <tr>
        <td align="center" colspan="2">後置條件：無</td>
    </tr>
    <tr>
        <td align="center" colspan="2">特別需求：2s書籤功能需登入才能使用</td>
    </tr>

</table>
